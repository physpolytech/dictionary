from time import time

# Существующие команды КСО
#
# eventId содержит новый префикс voi (voice), обозначающий инициатора события - голос/микрофон
# в круглых скобках указан номер шага в табличном представлении сценария (в командах КСО)
#
# На верхнем уровне указан этап (экран)
# На нижнем (втором) уровне указаны возможные команды и соответствующие им вызовы API
#
# У каждого состояния доступны команды из common (соответственно у состояний без команд - только из common)
commands = {
    'common': {
        # Вернуться назад: то же, что и тап на "Назад"
        'назад': {
            'action': 'cancel',
            'eventId': 'voi' + str(int(time()))
        },
        'вызвать ассистента': {
            'action': 'changeTopper',
            'scoState': 'sco_attendant',
            'eventId': 'voi' + str(int(time()))
        },
        'отменить покупку': {
            'action': 'cancelCheckScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'один': {
            'action': 'pinPadOne',
            'eventId': 'voi' + str(int(time()))
        },
        'два': {
            'action': 'pinPadTwo',
            'eventId': 'voi' + str(int(time()))
        },
        'три': {
            'action': 'pinPadThree',
            'eventId': 'voi' + str(int(time()))
        },
        'четыре': {
            'action': 'pinPadFour',
            'eventId': 'voi' + str(int(time()))
        },
        'пять': {
            'action': 'pinPadFive',
            'eventId': 'voi' + str(int(time()))
        },
        'шесть': {
            'action': 'pinPadSix',
            'eventId': 'voi' + str(int(time()))
        },
        'семь': {
            'action': 'pinPadSeven',
            'eventId': 'voi' + str(int(time()))
        },
        'восемь': {
            'action': 'pinPadEight',
            'eventId': 'voi' + str(int(time()))
        },
        'девять': {
            'action': 'pinPadNine',
            'eventId': 'voi' + str(int(time()))
        },
        'ноль': {
            'action': 'pinPadZero',
            'eventId': 'voi' + str(int(time()))
        },
        'удалить': {
            'action': 'pinPadDelete',
            'eventId': 'voi' + str(int(time()))
        },
        'применить': {
            'action': 'pinPadConfirm',
            'eventId': 'voi' + str(int(time()))
        }
    },
    'startScreen': {
        # В начале каждого сценария 2.1 (opt)
        'начать': {
            'action': 'startPurchase',
            'eventId': 'voi' + str(int(time()))
        },
    },
    'purchasesScreen': {
        # 2.1 Добавление весового товара (1)
        'взвесить': {
            'action': 'weighScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'выпечка': {
            'action': 'bakeryScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'добавить': {
            'action': 'typeBarCodeScreen',
            'eventId': 'voi' + str(int(time())),
        },
    },
    'cancelCheckScreen': {},
    'barCodeNotFoundScreen': {},
    'bakeryScreen': {
        'выпечка': {
            'action': 'selectBakeryTab',
            'eventId': 'voi' + str(int(time())),
        },
        'напитки': {
            'action': 'selectDrinksTab',
            'eventId': 'voi' + str(int(time())),
        }
    },
    'addBagsScreen': {
        # Добавление пакетов, переход в Подытог
        'нет спасибо': {
            'action': 'subTotalScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'плюс': {
            'action': 'plusOne',
            'eventId': 'voi' + str(int(time()))
        },
        'минус': {
            'action': 'minusOne',
            'eventId': 'voi' + str(int(time()))
        }
    },
    'subTotalScreen': {
        'скидки и купоны': {
            'action': 'chooseDiscountScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'банковская карта': {
            'action': 'payByCardScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'карта партнёра': {
            'action': 'cobrandCardScreen',
            'eventId': 'voi' + str(int(time()))
        },
        # 'социальная карта': {},
        'баллами с карты': {
            'action': 'payByLoyaltyPointsScreen',
            'eventId': 'voi' + str(int(time()))
        },
    },
    'chooseDiscountScreen': {
        'купон': {
            'action': 'promoCouponScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'выручай карта': {
            'action': 'loyaltyCardScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'скидки пенсионерам': {
            'action': 'pensionerDiscountScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'семейная среда': {
            'action': 'familyScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'карта сотрудника': {
            'action': 'employeeCardScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'вип карта': {
            'action': 'vipCardScreen',
            'eventId': 'voi' + str(int(time()))
        },
    },
    'promoCouponScreen': {},
    'loyaltyCardScreen': {},
    'employeeCardScreen': {},
    'vipCardScreen': {},
    'familyScreen': {},
    'pensionerDiscountScreen': {},
    'cobrandCardScreen': {},
    'payByLoyaltyPointsScreen': {
        'мобильное приложение': {
            'action': 'payByLoyaltyCardScreen',
            'eventId': 'voi' + str(int(time()))
        },
        'карта выручайка': {
            'action': 'payByLoyaltyMobileAppScreen',
            'eventId': 'voi' + str(int(time()))
        }
    },
    'enterLoyaltyPointsScreen': {
        'оплатить': {
            'action': 'payScreen',
            'eventId': 'voi' + str(int(time()))
        },
    },
    'payScreen': {},
    'payByCardScreen': {},
}
