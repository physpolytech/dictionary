from common.callAssistantMap import callAssistantMap
from common.cancelMap import cancelMap
from common.continueMap import continueMap
from common.pinPadMap import pinPadMap, pinPadPayMap, payMap
import copy
######################################################

# начальный экран

# команда "начать"
startScreenMap = dict.fromkeys(
    ['начать', 'вперед', 'старт', 'включись', 'привет'], 'начать')
startScreenMap.update(callAssistantMap)

######################################################

# экран "список покупок"

# команда "выпечка"
bakeryMap = dict.fromkeys(['выпечка', 'напитки'], 'выпечка')
# команда "добавить"
addItemMap = dict.fromkeys(
    ['добавить', 'ввести штрихкод', 'добавить товар', 'добавить покупку'], 'добавить')
# команда "отменить покупку"
cancelCheckMap = dict.fromkeys(
    ['отменить покупку', 'отменить чек'], 'отменить покупку')

# команда "взвесить"
purchasesScreenMap = dict.fromkeys(['взвесить', 'завесить'], 'взвесить')
purchasesScreenMap.update(bakeryMap)
purchasesScreenMap.update(continueMap)
purchasesScreenMap.update(addItemMap)
purchasesScreenMap.update(callAssistantMap)
purchasesScreenMap.update(cancelCheckMap)

######################################################

# экран "Отменить покупку (проверка)"
cancelCheckScreenMap = copy.copy(cancelMap)

######################################################

# экран "добавление товара" (ввод штрихкода)

typeBarCodeScreenMap = copy.copy(pinPadMap)
typeBarCodeScreenMap.update(cancelMap)
typeBarCodeScreenMap.update(callAssistantMap)

######################################################

# экран "добавление товара" (штрихкод не найден)
barCodeNotFoundScreenMap = copy.copy(cancelMap)
barCodeNotFoundScreenMap.update(callAssistantMap)

######################################################

# экран "товары на вес" (взвешивание)

# команда выбора вкладки "Овощи"
vegetablesMap = dict.fromkeys(['овощи'], 'овощи')
# команда выбора вкладки "Фрукты"
fruitsMap = dict.fromkeys(['фрукты'], 'фрукты')
# команда выбора вкладки "Конфеты"
sweetsMap = dict.fromkeys(['конфеты'], 'конфеты')

weighScreenMap = copy.copy(cancelMap)
weighScreenMap.update(sweetsMap)
weighScreenMap.update(callAssistantMap)
weighScreenMap.update(vegetablesMap)
weighScreenMap.update(fruitsMap)

######################################################

# экран выбора "выпечка/напитки/кофе"

# команда выбора вкладки "Выпечка"
bakeryTabMap = dict.fromkeys(['выпечка'], 'выпечка')
# команда выбора вкладки "Напитки"
drinksTabMap = dict.fromkeys(['напитки'], 'напитки')

bakeryScreenMap = copy.copy(cancelMap)
bakeryScreenMap.update(bakeryTabMap)
bakeryScreenMap.update(callAssistantMap)
bakeryScreenMap.update(drinksTabMap)

######################################################

# экран добавления пакетов

# Команда "Нет, спасибо"
skipBagMap = dict.fromkeys(['нет спасибо', 'не надо'], 'нет спасибо')
# Команда "+"
plusOneMap = dict.fromkeys(
    ['плюс', 'плюс один', 'добавить', 'прибавить'], 'плюс')
# Команда "-"
minusOneMap = dict.fromkeys(
    ['минус', 'минус один', 'убрать', 'отнять'], 'минус')

addBagsScreenMap = copy.copy(continueMap)
addBagsScreenMap.update(skipBagMap)
addBagsScreenMap.update(callAssistantMap)
addBagsScreenMap.update(plusOneMap)
addBagsScreenMap.update(minusOneMap)

######################################################

# экран "Подытог

# команда "Скидки и купоны"
discountsMap = dict.fromkeys(
    ['скидки', 'купоны', 'скидка', 'купон', 'скидки и купоны', 'применить купон', 'ввести купон'], 'скидки и купоны')
# команда "Банковской картой"
bankCardMap = dict.fromkeys(
    ['банковская карта', 'банковской картой', 'карта банка',
        'картой банка', 'кредитная карта', 'безнал'],
    'банковская карта')
# команда "Картой партнера"
coBrandCardMap = dict.fromkeys(
    ['карта партнёра', 'кобренд', 'картой партнёра', 'партнёрская карта'], 'карта партнёра')
# команда "Социальной картой"
# socCardMap = dict.fromkeys(
#    ['социальная карта', 'социальной картой', 'соц картой', 'соц карта', 'социалка', 'социалкой'], 'социальная карта')
# команда "Баллами с карты Выручайка"
loyaltyPointsMap = dict.fromkeys(
    ['баллами с карты', 'баллами', 'бонусами', 'выручайка', 'выручай', 'баллами с карты выручайка'], 'баллами с карты')

subTotalScreenMap = copy.copy(cancelCheckMap)
subTotalScreenMap.update(callAssistantMap)
subTotalScreenMap.update(discountsMap)
subTotalScreenMap.update(bankCardMap)
subTotalScreenMap.update(coBrandCardMap)
# subTotalScreenMap.update(socCardMap)
subTotalScreenMap.update(loyaltyPointsMap)

######################################################

# экран "выбор скидки"

# команда "Купон"
couponMap = dict.fromkeys(['купон', 'промокод', 'код', 'промо'], 'купон')
# команда "Выручай карта"
loyaltyCardMap = dict.fromkeys(
    ['выручай карта', 'выручайка', 'бонусная карта', 'карта лояльности'], 'выручай карта')
# команда "Скидки пенсионерам"
pensionerDiscountMap = dict.fromkeys(
    ['скидки пенсионерам', 'пенсионер', 'скидка пенсионеру'], 'скидки пенсионерам')
# команда "Семейная среда"
familyDiscountMap = dict.fromkeys(
    ['семейная среда', 'среда'], 'семейная среда')
# команда "Карта сотрудника"
employeeCardMap = dict.fromkeys(
    ['карта сотрудника', 'сотрудник'], 'карта сотрудника')
# команда "VIP-карта"
vipCardMap = dict.fromkeys(['вип карта', 'вип'], 'вип карта')

chooseDiscountScreenMap = copy.copy(cancelMap)
chooseDiscountScreenMap.update(callAssistantMap)
chooseDiscountScreenMap.update(couponMap)
chooseDiscountScreenMap.update(loyaltyCardMap)
chooseDiscountScreenMap.update(pensionerDiscountMap)
chooseDiscountScreenMap.update(familyDiscountMap)
chooseDiscountScreenMap.update(employeeCardMap)
chooseDiscountScreenMap.update(vipCardMap)

######################################################

# экран "Промокоды и купоны"

promoCouponScreenMap = copy.copy(cancelMap)
promoCouponScreenMap.update(callAssistantMap)
promoCouponScreenMap.update(pinPadMap)

######################################################

# экран "Карта лояльности"

loyaltyCardScreenMap = copy.copy(cancelMap)
loyaltyCardScreenMap.update(callAssistantMap)
loyaltyCardScreenMap.update(pinPadMap)

######################################################

# экран "Карта сотрудника"

employeeCardScreenMap = copy.copy(cancelMap)
employeeCardScreenMap.update(callAssistantMap)
employeeCardScreenMap.update(pinPadMap)

######################################################

# экран "Vip-карта"

vipCardScreenMap = copy.copy(cancelMap)
vipCardScreenMap.update(callAssistantMap)
vipCardScreenMap.update(pinPadMap)

######################################################

# экран "Семейная среда (проверка)"

familyScreenMap = copy.copy(cancelMap)
familyScreenMap.update(callAssistantMap)

######################################################

# экран "Скидка пенсионерам (проверка)"

pensionerDiscountScreenMap = copy.copy(cancelMap)
pensionerDiscountScreenMap.update(callAssistantMap)

######################################################

# экран "Оплата картой (карта партнера)"

cobrandCardScreenMap = copy.copy(cancelMap)
cobrandCardScreenMap.update(callAssistantMap)

######################################################

# экран "Оплата баллами (выбор способа)"

# команда "Оплатить через мобильное приложение"
payByLoyaltyPointsAppMap = dict.fromkeys(['мобильное приложение', 'приложение', 'телефон', 'смартфон'],
                                         'мобильное приложение')

# команда "Оплатить картой Выручайка"
payByLoyaltyPointsCardMap = dict.fromkeys(
    ['карта выручайка', 'выручай', 'выручайка', 'карта', 'карта лояльности', 'бонусная карта'], 'карта выручайка')

payByLoyaltyPointsScreenMap = copy.copy(cancelMap)
payByLoyaltyPointsScreenMap.update(callAssistantMap)
payByLoyaltyPointsScreenMap.update(payByLoyaltyPointsAppMap)
payByLoyaltyPointsScreenMap.update(payByLoyaltyPointsCardMap)

######################################################

# экран "Оплата баллами (карта выручайка)"

payByLoyaltyCardScreenMap = copy.copy(cancelMap)
payByLoyaltyCardScreenMap.update(callAssistantMap)

######################################################

# экран "Оплата баллами (мобильное приложение)"

payByLoyaltyMobileAppScreenMap = copy.copy(cancelMap)
payByLoyaltyMobileAppScreenMap.update(callAssistantMap)

######################################################

# экран "Списание баллов"

enterLoyaltyPointsScreenMap = copy.copy(cancelMap)
enterLoyaltyPointsScreenMap.update(pinPadPayMap)
enterLoyaltyPointsScreenMap.update(callAssistantMap)

######################################################

# экран "Итого к оплате"

payScreenMap = copy.copy(cancelMap)
payScreenMap.update(callAssistantMap)
payScreenMap.update(cancelCheckMap)
payScreenMap.update(payMap)

######################################################

# экран "Оплата покупок" (банковской картой)

payByCardScreenMap = copy.copy(cancelMap)
payByCardScreenMap.update(callAssistantMap)


# Печать в команд
dict1 = addBagsScreenMap
# print(dict1)
print(dict1.values())
print()
for i in set(dict1.values()):
    print(i, str([k for k, v in dict1.items() if v == i]
                 ).replace("', '", "|").replace("'", ""))
    print()
