import os
from pocketsphinx import LiveSpeech, get_model_path
import pyaudio
import copy
import sys
from utils import getkeyword


# def listen(screenname):
#     current_screenname = copy.copy(screenname)
#     p = pyaudio.PyAudio()
#     stream = p.open(format=pyaudio.paInt16, channels=1,
#                     rate=16000, input=True, frames_per_buffer=6000)
#     stream.start_stream()
#     screen_grammatic = screenname + '.jsgf'
#     model_path = get_model_path()

#     speech = LiveSpeech(
#         verbose=False,
#         sampling_rate=16000,
#         # buffer_size=2048,
#         no_search=False,
#         full_utt=False,
#         hmm='/model/zero_ru.cd_cont_4000',
#         lm=False,
#         jsgf='/model/'+screen_grammatic,
#         dic='/model/keys_compiled.dic'
#     )

#     print("Say something!")

#     for phrase in speech:
#         screenname = pass
#         if current_screenname != screenname:
#             break


# whie True:
#     listen(screenname)

# current_screenname = copy.copy(screenname)

screenname = 'startScreen'
# p = pyaudio.PyAudio()
# stream = p.open(format=pyaudio.paInt16, channels=1,
#                 rate=16000, input=True, frames_per_buffer=6000)
# stream.start_stream()
screen_grammatic = screenname + '.jsgf'
MODELDIR = '/model/'
model_path = get_model_path()

while(True):
    print(screenname)
    speech = LiveSpeech(
        verbose=False,
        sampling_rate=16000,
        buffer_size=128,
        no_search=False,
        full_utt=False,
        # hmm=os.path.join('/model/', 'zero_ru.cd_cont_4000'),
        # hmm=os.path.join(MODELDIR, 'zero_ru.cd_cont_4000'),
        # lm=False,
        # jsgf=os.path.join('/model/', screen_grammatic),
        # jsgf=os.path.join(MODELDIR, screen_grammatic),
        # dic=os.path.join('/model/', 'keys_compiled.dic'),
        # dic=os.path.join(MODELDIR, 'keys_compiled.dic'),

        hmm=os.path.join(model_path, 'zero_ru.cd_cont_4000'),
        lm=False,
        dic=os.path.join(model_path, 'keys_compiled.dic'),
        jsgf=os.path.join(model_path, screen_grammatic)
    )

    print("Say something!")

    for phrase in speech:
        print(phrase)
        keyword = getkeyword(phrase, screenname)
        json = getjson(keyword, screenname)
        print(json)
        if(json['action'] in commands):
            if screenname == json['action']:
                screenname = json['action']
                break
