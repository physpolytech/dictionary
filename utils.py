from dictionary import commands


def getkeyword(word, screenvar):
    keyword = screenvar[word]
    return keyword


def getjson(keyword, screenname):
    if keyword in commands['common']:
        json = commands['common'][keyword]
    elif keyword in commands[screenname]:
        json = commands[screenname][keyword]
    else:
        json = None
    return json


word = 'взвесить'
screenname = 'purchasesScreen'

screenvar = locals().get(screenname + 'Map')
print(getjson(getkeyword(word, screenvar), screenname))
